#include <unistd.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char o_file[100];
int PAGELEN;	
int LINELEN;


int found = 0;
int print;
float lines_of_file;
float read_lines;
void do_more(FILE *);
int  get_input(FILE*);
int main(int argc , char *argv[])
{
  
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   int i=0;
   if (argc == 1){
      do_more(stdin);
   }
   FILE * fp;
   while(++i < argc){
      lines_of_file = 0;
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      char buffer[LINELEN];
      int len = 0;
      while (fgets(buffer, LINELEN, fp)!=NULL){
	 lines_of_file=lines_of_file+1;
	 
	 len=len+strlen(buffer);
      }
      fseek(fp,-len,SEEK_CUR);
      strcpy(o_file,argv[i]);
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   int num_of_lines = 0;
   read_lines=0;
   int rv;
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   while (fgets(buffer, LINELEN, fp)){
      if(num_of_lines<PAGELEN){
	read_lines=read_lines+1;
	found = 0;
      	fputs(buffer, stdout);
      	num_of_lines=num_of_lines+1;
      }
      if (num_of_lines == PAGELEN){
	 system("stty -icanon");
	 system("stty -echo");
         
         if(print!=1) {
		float per=read_lines/lines_of_file;
		per = per*100;
		printf("\033[7m --more--(%d%c)\033[m",(int)per,'%');
		
	 }
         rv = get_input(fp_tty);
	 system("stty icanon");
         system("stty echo");
         if (rv == 0){//user pressed q
            printf(" \033[1B \033[2K \033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
	    print = 0;
            printf("\033[1B \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){//user pressed return/enter
	    if(print == 1){
		printf("\033[2K \033[1G");
		fputs(buffer, stdout);
		print = 0;
	    }
            else{printf("\033[1B  \033[2K \033[1G");
	         num_of_lines -= 1;} //show one more line
         }
         else if (rv == 3){ //search
            printf("\033[2K \033[0G/");
            char str[30];
	    scanf("%s",str);
	    char buff[30];
	    int len = 0;
	    while(fgets(buff,30, fp)!=NULL){
		int N=strlen(buff);
		int M=strlen(str);
		for (int i = 0; i <= N - M; i++) { 
		int j; 
		/* For current index i, check for pattern match */
			for (j = 0; j < M; j++) 
				if (buff[i + j] != str[j]) 
					break; 
			if (j == M) 
				found = 1; 
	        }   
		if(found == 1){
			printf("\n\033[1A \033[2K \033[0GSkipping...\n");
			fseek(fp,-3*strlen(buff),SEEK_CUR);
			num_of_lines -= PAGELEN;
			print = 0;
			len = 0;
			break;
		}
		len=len+strlen(buff);
	    }
	    if(found == 0){
		printf("\033[1A\033[7mPattern Not Found \033[m");
		print=1;
		fseek(fp,-len,SEEK_CUR);
		len = 0;
	    } 
         }
        else if(rv == 4){	
                int pid = fork();
               if(pid == 0);
        		int ret=execl("/usr/bin/vim","vim",o_file,(char*)0);
        }
	else if(rv == 5){printf("\033[1A \033[2K \033[1G");exit(0);}
      }
  }
}

int get_input(FILE* cmdstream)
{
   
   int c;		
     c = getc(cmdstream);
      if(c == 'q')
	 return 0;
      if ( c == ' ' )			
	 return 1;
      if ( c == '\n' )	
	 return 2;
      if ( c == '/' )	
          return 3;
      if ( c == 'v' )	
          return 4;
   return 4;
}