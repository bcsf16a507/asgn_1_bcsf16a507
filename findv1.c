#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int isDir(const char *path) {
   struct stat buff;
   if (stat(path, &buff) != 0)
       return 0;
   return S_ISDIR(buff.st_mode);
}

int find(char *path, char *file){
	DIR * dp = opendir(path);
	chdir(path);
	struct dirent* entry;
	while(1) {
		entry = readdir(dp);
	        if(entry == NULL){
		    return 0;
	        }
		//ignore hidden files
		if(entry->d_name[0] == '.'){
		    continue;
		}
		char Path[1024] = "";
		strcat(Path, path);
		strcat(Path, "/");
		strcat(Path, entry->d_name);
		if(strcmp(entry->d_name, file) == 0){
			printf("%s \n", Path);
		}
		if(isDir(Path) == 1) {
			find(Path, file);
		}
	}
	closedir(dp);
}

int main(int argc, char * argv[]){
	char path[1024];
	getcwd(path, sizeof(path));
	if (argc == 4){
		find(argv[1], argv[3]);
	}
	else{
		find(path, argv[2]);
	}
   return 0;
}