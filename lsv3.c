#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <pwd.h>
void show_stat_info(const char*,char*);

extern int errno;
void do_ls(char*);
int compare(const void *a,const void *b){
	return strcmp(*(const char **)a,*(const char **)b);
}
int main(int argc, char* argv[]){
   if (argc == 1){
         printf("Directory listing of pwd:\n");
      do_ls(".");
   }
   else{
      int i = 0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}


void do_ls(char * dir)
{
   struct dirent * entry;
   struct passwd *pwd;
   struct group *grp;
   struct stat info;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   errno = 0;
   while((entry = readdir(dp)) != NULL){
         if(entry == NULL && errno != 0){
  		perror("readdir failed");
		exit(1);
  	}else{
                if(entry->d_name[0] == '.')
                    continue;
        }    
        
    struct stat buff;
   if (stat(entry->d_name, &buff)<0){
      perror("Error in stat");
      exit(1);
   }
   
   int rv = stat(entry->d_name, &info);
   if (rv == -1){
      perror("stat failed");
      exit(1);
   }
   printf("%lo ", info.st_ino);
   int mode = buff.st_mode; 
   char str[10];
   strcpy(str, "---------");
//owner  permissions
   if((mode & 0000400) == 0000400) str[0] = 'r';
   if((mode & 0000200) == 0000200) str[1] = 'w';
   if((mode & 0000100) == 0000100) str[2] = 'x';
//group permissions
   if((mode & 0000040) == 0000040) str[3] = 'r';
   if((mode & 0000020) == 0000020) str[4] = 'w';
   if((mode & 0000010) == 0000010) str[5] = 'x';
//others  permissions
   if((mode & 0000004) == 0000004) str[6] = 'r';
   if((mode & 0000002) == 0000002) str[7] = 'w';
   if((mode & 0000001) == 0000001) str[8] = 'x';
//special  permissions
   if((mode & 0004000) == 0004000) str[2] = 's';
   if((mode & 0002000) == 0002000) str[5] = 's';
   if((mode & 0001000) == 0001000) str[8] = 't';
   printf("%s ", str);  
   printf("%o ", info.st_mode);
   printf("%ld ", info.st_nlink);
   pwd = getpwuid(info.st_uid);
   printf("%s ",pwd->pw_name);
   printf("%d ",info.st_gid);
    char *t=ctime(&info.st_mtime);
   t[strlen(t)-1]='\0';
   printf("%ld ", info.st_size);
   printf("%s %s ",t+4,entry->d_name); 
   printf("\n");
             
}   
   closedir(dp);
}
