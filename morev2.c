#include <unistd.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int PAGELEN;	
int LINELEN;


int found = 0;
int print;
float lines_of_file;
float read_lines;
void do_more(FILE *);
int  get_input(FILE*);
int main(int argc , char *argv[])
{
  
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   int i=0;
   if (argc == 1){
      do_more(stdin);
   }
   FILE * fp;
   while(++i < argc){
      lines_of_file = 0;
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      char buffer[LINELEN];
      int len = 0;
      while (fgets(buffer, LINELEN, fp)!=NULL){
	 lines_of_file=lines_of_file+1;
	 
	 len=len+strlen(buffer);
      }
      fseek(fp,-len,SEEK_CUR);
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   int num_of_lines = 0;
   read_lines=0;
   int rv;
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   while (fgets(buffer, LINELEN, fp)){
      if(num_of_lines<PAGELEN){
	read_lines=read_lines+1;
	found = 0;
      	fputs(buffer, stdout);
      	num_of_lines=num_of_lines+1;
      }
      if (num_of_lines == PAGELEN){
	 
		float per=read_lines/lines_of_file;
		per = per*100;
		printf("\033[7m --more--(%d%c)\033[m",(int)per,'%');
		
         rv = get_input(fp_tty);
	 
         if (rv == 0){//user pressed q
            printf(" \033[1A \033[2K \033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
	    print = 0;
            printf("\033[1A \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){//user pressed return/enter
		printf("\033[1A  \033[2K \033[1G");
	         num_of_lines -= 1;} //show one more line
	else if(rv == 3){printf("\033[1A \033[2K \033[1G");exit(0);}
      }
  }
}

int get_input(FILE* cmdstream)
{
   
   int c;		
     c = getc(cmdstream);
      if(c == 'q')
	 return 0;
      if ( c == ' ' )			
	 return 1;
      if ( c == '\n' )	
	 return 2;
      
   return 3;
}