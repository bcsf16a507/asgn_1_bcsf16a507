#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int isDir(const char *path) {
   struct stat buff;
   if (stat(path, &buff) != 0)
       return 0;
   return S_ISDIR(buff.st_mode);
}

int find(char *path, char *type, char * str){
	DIR * dp = opendir(path);
	chdir(path);
	errno = 0;
        while(1){
	struct dirent* entry;
		entry = readdir(dp);
	        if(entry == NULL && errno == 0){
		    closedir(dp);
		    return 0;
	        }
		//skip hidden files
		if(entry->d_name[0] == '.'){
		    continue;
		}
		char Path[1024] = "";
		strcat(Path, path);
		strcat(Path, "/");
		strcat(Path, entry->d_name);
		struct stat f;
		stat(Path, &f);
		if(strcmp(type,"-type")==0){
			if(strcmp(str,"f")){
           			if (S_ISREG(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}
                	if(strcmp(str,"d")){
				printf("hello");
           			if (S_ISDIR(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}
	        	if(strcmp(str,"b")){
           			if (S_ISBLK(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}
			if(strcmp(str,"c")){
           			if (S_ISCHR(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}
			if(strcmp(str,"p")){
           			if (S_ISFIFO(f.st_mode)) {
					printf("%s \n", Path);
				}		
			}
	        	if(strcmp(str,"l")){
           			if (S_ISLNK(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}
			if(strcmp(str,"s")){
           			if (S_ISSOCK(f.st_mode)) {
					printf("%s \n", Path);
				}	
			}

           }
	  else if(strcmp(type,"-name")==0){
			if(strcmp(entry->d_name,str) == 0){
				printf("%s \n", Path);
			}
			
          }
	 if(isDir(Path) == 1) {
		find(Path,type,str);
	  }
     } 
    closedir(dp);
}

int main(int argc, char * argv[]){
	char path[1024];
        char *type;
	char *str;
	getcwd(path, sizeof(path));
	if(argc==4){
		if(strcmp(argv[2], "-type") == 0) {
			str = argv[3];
			type = "-type";
	        }
		if(strcmp(argv[2], "-name") == 0) {
			type = "-name";
			str = argv[3];
                }
	}
        else{
		if(strcmp(argv[1], "-type") == 0) {
			type = "-type";
			str = argv[2];
	        }
		if(strcmp(argv[1], "-name") == 0) {
			type = "-name";
			str = argv[2];
                }
	}
	if(strcmp(argv[1], ".") != 0){
		find(argv[1],type,str);
	}
	else{
		find(path,type,str);
	}
   return 0;
}