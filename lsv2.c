#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>

extern int errno;
void do_ls(char*);
int main(int argc, char* argv[]){
   if (argc == 1){
         printf("Directory listing of pwd:\n");
      do_ls(".");
   }
   else{
      int i = 0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}
int compare(const void *a,const void *b){
	return strcmp(*(const char **)a,*(const char **)b);
}

void do_ls(char * dir)
{
   struct dirent * entry;
   char * array[1000];
   int i = 0;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   errno = 0;
   while((entry = readdir(dp)) != NULL){
         if(entry == NULL && errno != 0){
  		perror("readdir failed");
		exit(1);
  	}else{
                if(entry->d_name[0] == '.')
                    continue;
        }     	
		
		array[i]=entry->d_name;;
               i++;       
   }
   qsort(array,i,sizeof(const char *),compare);
   for(int j=0;j<i;j++){
   	printf("%s\n",array[j]);
   }
   closedir(dp);
}